﻿using MatrixProject.Api.Controllers;
using MatrixProject.Api.Enums;
using MatrixProject.Api.Models.Response;
using MatrixProject.Api.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace MatrixProject.Api.Tests.Controllers
{
    public class DistanceControllerTests
    {
        private readonly Mock<IDistanceService> distanceService;

        private readonly DistanceController controller;

        public DistanceControllerTests()
        {
            this.distanceService = new Mock<IDistanceService>();

            this.controller = new DistanceController(distanceService.Object);
        }

        string origin = "My address!";
        string destination = "I'm going to here!";
        TravelMode mode = TravelMode.Walking;

        [Fact]
        public async Task GetDistance_CallsService()
        {
            // Arrange
            var response = new DistanceResponse();

            distanceService
                .Setup(x => x.GetDistanceBetweenLocationsAsync(origin, destination, mode))
                .ReturnsAsync(response);

            // Act
            var output = await controller.GetDistance(origin, destination, mode);

            // Assert
            Assert.Equal(response, ((OkObjectResult)output).Value);

            distanceService
                .Verify(x => x.GetDistanceBetweenLocationsAsync(origin, destination, mode),
            Times.Once);
        }

        [Fact]
        public async Task GetDistance_CallsService_DefaultToDrivingMode()
        {
            // Arrange
            var response = new DistanceResponse();

            distanceService
                .Setup(x => x.GetDistanceBetweenLocationsAsync(origin, destination, TravelMode.Driving))
                .ReturnsAsync(response);

            // Act
            await controller.GetDistance(origin, destination);

            // Assert
            distanceService
                .Verify(x => x.GetDistanceBetweenLocationsAsync(origin, destination, TravelMode.Driving),
            Times.Once);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task GetDistance_InvalidOrigin_BadRequest(string origin)
        {
            // Act
            var output = await controller.GetDistance(origin, destination);

            // Assert
            Assert.IsType<BadRequestResult>(output);

            distanceService
                .Verify(x => x.GetDistanceBetweenLocationsAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TravelMode>()),
            Times.Never);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public async Task GetDistance_InvalidDestination_BadRequest(string destination)
        {
            // Act
            var output = await controller.GetDistance(origin, destination);

            // Assert
            Assert.IsType<BadRequestResult>(output);

            distanceService
                .Verify(x => x.GetDistanceBetweenLocationsAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<TravelMode>()),
            Times.Never);
        }
    }
}
