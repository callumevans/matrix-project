﻿using MatrixProject.Api.Clients.Interfaces;
using MatrixProject.Api.Enums;
using MatrixProject.Api.Extensions;
using MatrixProject.Api.Models.Response;
using MatrixProject.Api.Services;
using MatrixProject.Api.Services.Interfaces;
using Moq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace MatrixProject.Api.Tests.Services
{
    public class DistanceServiceTests
    {
        private readonly Mock<IDistanceApiClient> distanceApiClient;
        private readonly Mock<IResponseParserService> responseParser;

        private readonly DistanceService service;

        public DistanceServiceTests()
        {
            this.distanceApiClient = new Mock<IDistanceApiClient>();
            this.responseParser = new Mock<IResponseParserService>();

            this.service = new DistanceService(
                distanceApiClient.Object,
                responseParser.Object);
        }

        string origin = "my origin";
        string destination = "my destination";
        TravelMode mode = TravelMode.Driving;

        [Fact]
        public async Task GetDistanceBetweenLocations_CallsApi()
        {
            // Arrange
            var message = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK
            };

            var distance = new DistanceResponse();

            distanceApiClient
                .Setup(x => x.ExecuteAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(message);

            responseParser
                .Setup(x => x.ParseAsync<DistanceResponse>(message))
                .ReturnsAsync(distance);

            // Act
            var result = await service.GetDistanceBetweenLocationsAsync(origin, destination, mode);

            // Assert
            Assert.Equal(distance, result);

            distanceApiClient
                .Verify(x => x.ExecuteAsync(
                    It.Is<HttpRequestMessage>(i =>
                        i.QueryParameters()["origins"] == origin &&
                        i.QueryParameters()["destinations"] == destination)),
            Times.Once);
        }

        [Theory]
        [InlineData(HttpStatusCode.BadRequest)]
        [InlineData(HttpStatusCode.NotFound)]
        [InlineData(HttpStatusCode.InternalServerError)]
        public async Task GetDistanceBetweenLocations_Not200_ReturnsNull(HttpStatusCode status)
        {
            // Arrange
            var message = new HttpResponseMessage
            {
                StatusCode = status
            };

            var distance = new DistanceResponse();

            distanceApiClient
                .Setup(x => x.ExecuteAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(message);

            // Act
            var result = await service.GetDistanceBetweenLocationsAsync(origin, destination, mode);

            // Assert
            Assert.Null(result);
        }
    }
}
