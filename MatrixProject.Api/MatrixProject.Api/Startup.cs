﻿using MatrixProject.Api.Clients;
using MatrixProject.Api.Clients.Interfaces;
using MatrixProject.Api.Options;
using MatrixProject.Api.Services;
using MatrixProject.Api.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;

namespace MatrixProject.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddMvc();

            // Options
            services.Configure<DistanceApiOptions>(Configuration.GetSection("DistanceApiOptions"));

            // Dependencies
            services.AddTransient<HttpClient, HttpClient>();
            services.AddTransient<IDistanceApiClient, DistanceApiClient>();
            services.AddTransient<IResponseParserService, ResponseParserService>();
            services.AddTransient<IDistanceService, DistanceService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(builder =>
            builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseMvc();
        }
    }
}
