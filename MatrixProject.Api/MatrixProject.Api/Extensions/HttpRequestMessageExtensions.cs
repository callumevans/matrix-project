﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace MatrixProject.Api.Extensions
{
    public static class HttpRequestMessageExtensions
    {
        public static HttpRequestMessage AddQueryParameter(
            this HttpRequestMessage message, string name, object value)
        {
            string uri = QueryHelpers.AddQueryString(
                message.RequestUri?.ToString() ?? "", 
                name, 
                value.ToString());

            message.RequestUri = new Uri(uri, UriKind.Relative);

            return message;
        }

        public static Dictionary<string, StringValues> QueryParameters(
            this HttpRequestMessage message)
        {
            var dictionary = QueryHelpers.ParseNullableQuery(
                message.RequestUri.ToString());

            return dictionary;
        }
    }
}
