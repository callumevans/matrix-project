﻿namespace MatrixProject.Api.Enums
{
    public enum TravelMode
    {
        Driving = 1,
        Walking = 2,
        Bicycling = 3
    }
}
