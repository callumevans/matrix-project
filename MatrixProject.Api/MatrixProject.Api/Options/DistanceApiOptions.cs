﻿namespace MatrixProject.Api.Options
{
    public class DistanceApiOptions
    {
        public string GoogleDistanceApi { get; set; }

        public string ApiKey { get; set; }
    }
}
