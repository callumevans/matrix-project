﻿using MatrixProject.Api.Enums;
using MatrixProject.Api.Models.Response;
using MatrixProject.Api.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MatrixProject.Api.Controllers
{
    [Route("distance")]
    public class DistanceController : Controller
    {
        private readonly IDistanceService distanceService;

        public DistanceController(IDistanceService distanceService)
        {
            this.distanceService = distanceService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetDistance(
            [FromQuery]string origin, 
            [FromQuery]string destination,
            [FromQuery]TravelMode mode = TravelMode.Driving)
        {
            if (string.IsNullOrWhiteSpace(origin) || string.IsNullOrWhiteSpace(destination))
            {
                return BadRequest();
            }

            DistanceResponse result = await distanceService.GetDistanceBetweenLocationsAsync(
                origin, destination, mode);

            return Ok(result);
        }
    }
}
