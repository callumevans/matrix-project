﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MatrixProject.Api.Services.Interfaces
{
    public interface IResponseParserService
    {
        Task<T> ParseAsync<T>(HttpResponseMessage response);
    }
}
