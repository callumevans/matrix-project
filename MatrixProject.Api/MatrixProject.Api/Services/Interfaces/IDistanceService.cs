﻿using MatrixProject.Api.Enums;
using MatrixProject.Api.Models.Response;
using System.Threading.Tasks;

namespace MatrixProject.Api.Services.Interfaces
{
    public interface IDistanceService
    {
        Task<DistanceResponse> GetDistanceBetweenLocationsAsync(
            string origin, string destination, TravelMode mode);
    }
}
