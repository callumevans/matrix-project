﻿using System.Net.Http;
using System.Threading.Tasks;
using MatrixProject.Api.Services.Interfaces;
using Newtonsoft.Json;

namespace MatrixProject.Api.Services
{
    public class ResponseParserService : IResponseParserService
    {
        public async Task<T> ParseAsync<T>(HttpResponseMessage response)
        {
            string content = await response.Content.ReadAsStringAsync();

            T result = JsonConvert.DeserializeObject<T>(content);

            return result;
        }
    }
}
