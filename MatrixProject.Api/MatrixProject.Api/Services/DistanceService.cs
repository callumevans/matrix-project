﻿using MatrixProject.Api.Clients.Interfaces;
using MatrixProject.Api.Enums;
using MatrixProject.Api.Extensions;
using MatrixProject.Api.Models.Response;
using MatrixProject.Api.Services.Interfaces;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatrixProject.Api.Services
{
    public class DistanceService : IDistanceService
    {
        private readonly IDistanceApiClient distanceApiClient;
        private readonly IResponseParserService responseParser;

        public DistanceService(
            IDistanceApiClient distanceApiClient,
            IResponseParserService responseParser)
        {
            this.distanceApiClient = distanceApiClient;
            this.responseParser = responseParser;
        }

        public async Task<DistanceResponse> GetDistanceBetweenLocationsAsync(
            string origin, string destination, TravelMode mode)
        {
            var request = new HttpRequestMessage();

            request.AddQueryParameter("origins", origin);
            request.AddQueryParameter("destinations", destination);

            string modeQuery = string.Empty;

            switch (mode)
            {
                case TravelMode.Bicycling:
                    modeQuery = "bicycling";
                    break;
                case TravelMode.Walking:
                    modeQuery = "walking";
                    break;
                case TravelMode.Driving:
                default:
                    modeQuery = "driving";
                    break;
            }

            request.AddQueryParameter("mode", modeQuery);

            var result = await distanceApiClient.ExecuteAsync(request);

            if (result.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            return await responseParser.ParseAsync<DistanceResponse>(result);
        }
    }
}
