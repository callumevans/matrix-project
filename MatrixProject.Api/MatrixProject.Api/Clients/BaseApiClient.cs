﻿using MatrixProject.Api.Clients.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatrixProject.Api.Clients
{
    public abstract class BaseApiClient : IBaseApiClient
    {
        protected readonly HttpClient client;

        public BaseApiClient(HttpClient client)
        {
            this.client = client;
        }

        public virtual async Task<HttpResponseMessage> ExecuteAsync(HttpRequestMessage request)
        {
            HttpResponseMessage response = await client.SendAsync(request);

            return response;
        }
    }
}
