﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MatrixProject.Api.Clients.Interfaces
{
    public interface IBaseApiClient
    {
        Task<HttpResponseMessage> ExecuteAsync(HttpRequestMessage request);
    }
}
