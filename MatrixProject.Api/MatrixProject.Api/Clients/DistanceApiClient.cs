﻿using MatrixProject.Api.Clients.Interfaces;
using MatrixProject.Api.Extensions;
using MatrixProject.Api.Options;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatrixProject.Api.Clients
{
    public class DistanceApiClient : BaseApiClient, IDistanceApiClient
    {
        private readonly string apiKey;

        public DistanceApiClient(
            HttpClient client, 
            IOptionsSnapshot<DistanceApiOptions> distanceApiOptions) 
            : base(client)
        {
            this.client.BaseAddress = new Uri(distanceApiOptions.Value.GoogleDistanceApi);
            this.apiKey = distanceApiOptions.Value.ApiKey;
        }

        public override async Task<HttpResponseMessage> ExecuteAsync(HttpRequestMessage request)
        {
            request.AddQueryParameter("key", apiKey);

            return await base.ExecuteAsync(request);
        }
    }
}
