﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MatrixProject.Api.Models.Response
{
    public class DistanceResponse
    {
        [JsonProperty(PropertyName = "destination_addresses")]
        public List<string> DestinationAddresses { get; set; }

        [JsonProperty(PropertyName = "origin_addresses")]
        public List<string> OriginAddresses { get; set; }

        public dynamic Rows { get; set; }
    }
}
