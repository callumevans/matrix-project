Vue.component('request-runner', {
    template: '<p v-if="distance && duration">Distance: {{distance}}. Duration: {{duration}}.</p>',
    created: function () {
        eventHub.$on('newRequest', this.runRequest);
        eventHub.$on('runRequest', this.runRequest);
    },
    methods: {
        runRequest: function (request) {
            this.$http.get('http://localhost:5000/distance', { params: {
                origin: request.origin,
                destination: request.destination,
                mode: request.mode }})
            .then(function (response) {
                this.distance = response.body.rows[0].elements[0].distance.text;
                this.duration = response.body.rows[0].elements[0].duration.text;
            });
        }
    },
    data: function () {
        return {
            distance: null,
            duration: null
        }
    }
});