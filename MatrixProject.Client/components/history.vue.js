Vue.component('history-list', {
    template:   '<ul id="request-history">' +
                '<li v-for="request in requests">{{request.origin}} to {{request.destination}} by {{request.friendlyMode}} <button v-on:click="runRequest(request)" type="button" class="btn btn-sm btn-info">Run</button></li>' +
                '</ul>',
    created: function () {
        eventHub.$on('newRequest', this.addToHistory)
    },
    methods: {
        addToHistory: function (request) {
            var friendlyMode = null;
            switch (request.mode) {
                default:
                case '1':
                    friendlyMode = 'Driving';
                    break;
                case '2':
                    friendlyMode = 'Walking';
                    break;
                case '3':
                    friendlyMode = 'Cycling';
                    break;
            }

            request.friendlyMode = friendlyMode;
            this.requests.push(request);
        },
        runRequest: function (request) {
            eventHub.$emit('runRequest', request);
        }
    },
    data: function () {
        return {
            requests: []
        }
    }
});