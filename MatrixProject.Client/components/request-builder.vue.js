Vue.component('request-builder', {
    template:   '<form>' +
                '<div class="form-group"><input class="form-control" v-model="origin" placeholder="Origin" type="text" /></div>' +
                '<div class="form-group"><input class="form-control" v-model="destination" placeholder="Destination" type="text" /></div>' +
                '<div class="form-group"><select class="form-control" v-model="mode"><option value="1">Driving</option><option value="2">Walking</option><option value="3">Cycling</option></select></div>' +
                '<div class="form-group"><button class="btn btn-primary" v-on:click="newRequest" type="button">Submit</button></div>' +
                '</form>',
    methods: {
        newRequest: function () {
            eventHub.$emit('newRequest', {
                origin: this.origin,
                destination: this.destination,
                mode: this.mode
            });
        }
    },
    data: function () {
        return {
            origin: null,
            destination: null,
            mode: null
        }
    }
});